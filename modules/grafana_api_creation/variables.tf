variable "project_name" {
  type = string
}

variable "grafana" {
  type = object({ url : string, api_key : string })
}
