module "mon_suivi_social" {
  source               = "./generic"
  common               = local.common
  project_name         = "Monsuivisocial"
  with_record          = false
  with_wildcard_record = false
}

resource "scaleway_domain_record" "monsuivisocial" {
  dns_zone = var.dns_zone_incubateur
  name     = "monsuivisocial"
  type     = "A"
  data     = var.production_haproxy_ip
  ttl      = 3600
}

resource "scaleway_domain_record" "monsuivisocial_wildcard" {
  dns_zone = var.dns_zone_incubateur
  name     = "*.monsuivisocial"
  type     = "CNAME"
  data     = var.production_cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "monsuivisocial_ovh_autodiscover" {
  dns_zone = var.dns_zone_incubateur
  name     = "_autodiscover._tcp.monsuivisocial"
  type     = "SRV"
  data     = "0 0 443 pro1.mail.ovh.net."
}

resource "scaleway_domain_record" "monsuivisocial_ovh_mx0" {
  dns_zone = var.dns_zone_incubateur
  name     = "monsuivisocial"
  type     = "MX"
  data     = "mx0.mail.ovh.net."
  priority = 1
}
resource "scaleway_domain_record" "monsuivisocial_ovh_mx1" {
  dns_zone = var.dns_zone_incubateur
  name     = "monsuivisocial"
  type     = "MX"
  data     = "mx1.mail.ovh.net."
  priority = 5
}
resource "scaleway_domain_record" "monsuivisocial_ovh_mx2" {
  dns_zone = var.dns_zone_incubateur
  name     = "monsuivisocial"
  type     = "MX"
  data     = "mx2.mail.ovh.net."
  priority = 50
}
resource "scaleway_domain_record" "monsuivisocial_ovh_mx3" {
  dns_zone = var.dns_zone_incubateur
  name     = "monsuivisocial"
  type     = "MX"
  data     = "mx3.mail.ovh.net."
  priority = 100
}
