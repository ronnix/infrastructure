locals {
  francethd_domain       = "francethd.fr"
  agencenumerique_domain = "agencedunumerique.gouv.fr"
}

resource "scaleway_domain_record" "francethd_prod_record" {
  dns_zone = local.francethd_domain
  name     = ""
  type     = "ALIAS"
  data     = local.common.production_cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "francethd_prod_record_wildcard" {
  dns_zone = local.francethd_domain
  name     = "*"
  type     = "CNAME"
  data     = local.common.production_cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "agencenumerique_prod_record" {
  dns_zone = local.agencenumerique_domain
  name     = ""
  type     = "ALIAS"
  data     = local.common.production_cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "agencenumerique_prod_record_wildcard" {
  dns_zone = local.agencenumerique_domain
  name     = "*"
  type     = "CNAME"
  data     = local.common.production_cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "francethd_dev_record" {
  dns_zone = local.francethd_domain
  name     = "dev"
  type     = "CNAME"
  data     = local.common.development_cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "francethd_dev_record_wildcard" {
  dns_zone = local.francethd_domain
  name     = "*.dev"
  type     = "CNAME"
  data     = local.common.development_cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "agencenumerique_dev_record" {
  dns_zone = local.agencenumerique_domain
  name     = "dev"
  type     = "CNAME"
  data     = local.common.development_cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "agencenumerique_dev_record_wildcard" {
  dns_zone = local.agencenumerique_domain
  name     = "*.dev"
  type     = "CNAME"
  data     = local.common.development_cluster_cname
  ttl      = 3600
}

module "amenagement_numerique" {
  source       = "./generic"
  common       = local.common
  project_name = "Amenagement numerique"
}
