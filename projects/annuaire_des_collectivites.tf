module "annuaire_des_collectivites" {
  source       = "./generic"
  common       = local.common
  project_name = "AnnuaireDesCollectivites"
  project_slug = "annuaire-des-collectivites"
}

moved {
  from = module.annuaire_des_territoires
  to   = module.annuaire_des_collectivites
}
