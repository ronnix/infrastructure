resource "scaleway_domain_record" "sit" {
  dns_zone = var.dns_zone_incubateur
  name     = "sit"
  type     = "A"
  data     = var.sit_vm_ip
  ttl      = 3600
}
resource "scaleway_domain_record" "donnees" {
  dns_zone = var.dns_zone_incubateur
  name     = "donnees"
  type     = "A"
  data     = var.sit_vm_ip
  ttl      = 3600
}

module "donnees_et_territoires" {
  source            = "./generic"
  common            = local.common
  project_name      = "Donnees Et Territoires"
  project_slug      = "donneesetterritoires"
  project_subdomain = "donnees"
  with_record       = false
}

resource "scaleway_domain_record" "donnees_gitlab_pages" {
  dns_zone = var.dns_zone_incubateur
  name     = "_gitlab-pages-verification-code.donnees"
  type     = "TXT"
  data     = "gitlab-pages-verification-code=6c11661347a05505ba4b770a95fc2724"
}
resource "scaleway_domain_record" "donnees_sit" {
  dns_zone = var.dns_zone_incubateur
  name     = "*.sit"
  type     = "A"
  data     = "51.159.153.132"
  ttl      = 60
}
