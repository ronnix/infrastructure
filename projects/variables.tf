data "gitlab_group" "projects" {
  group_id = 15577471
}

variable "dns_zone_incubateur" {
  type = string
}
variable "production_cluster_cname" {
  type = string
}
variable "development_cluster_cname" {
  type = string
}

variable "development_base_domain" {
  type = string
}
variable "production_base_domain" {
  type = string
}
variable "scaleway_organization_id" {
  type = string
}
variable "scaleway_cluster_development_cluster_id" {
  type = string
}
variable "scaleway_cluster_production_cluster_id" {
  type = string
}

variable "grafana_production_url" {
  type = string
}
variable "grafana_production_auth" {
  type = string
}
variable "grafana_production_loki_url" {
  type = string
}
variable "grafana_production_prometheus_url" {
  type = string
}

variable "grafana_development_url" {
  type = string
}
variable "grafana_development_auth" {
  type = string
}
variable "grafana_development_loki_url" {
  type = string
}
variable "grafana_development_prometheus_url" {
  type = string
}

variable "sit_vm_ip" {
  type = string
}

variable "production_haproxy_ip" {
  type = string
}

locals {
  common = {
    projects_group_id                       = data.gitlab_group.projects.id
    dns_zone_incubateur                     = var.dns_zone_incubateur
    production_cluster_cname                = var.production_cluster_cname
    development_cluster_cname               = var.development_cluster_cname
    development_base_domain                 = var.development_base_domain
    production_base_domain                  = var.production_base_domain
    scaleway_organization_id                = var.scaleway_organization_id
    scaleway_cluster_development_cluster_id = var.scaleway_cluster_development_cluster_id
    scaleway_cluster_production_cluster_id  = var.scaleway_cluster_production_cluster_id
    grafana_production_url                  = var.grafana_production_url
    grafana_production_auth                 = var.grafana_production_auth
    grafana_production_loki_url             = var.grafana_production_loki_url
    grafana_production_prometheus_url       = var.grafana_production_prometheus_url
    grafana_development_url                 = var.grafana_development_url
    grafana_development_auth                = var.grafana_development_auth
    grafana_development_loki_url            = var.grafana_development_loki_url
    grafana_development_prometheus_url      = var.grafana_development_prometheus_url

  }
}
