resource "scaleway_domain_record" "search" {
  dns_zone = var.dns_zone_incubateur
  name     = "search"
  type     = "CNAME"
  data     = var.production_cluster_cname
  ttl      = 3600
}

module "incubator_website" {
  source               = "./generic"
  common               = local.common
  project_name         = "Site web incubateur"
  project_slug         = "incubator_website"
  with_record          = false
  with_wildcard_record = false
}
