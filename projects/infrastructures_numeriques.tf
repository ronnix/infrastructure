resource "scaleway_domain_record" "infrastructures_numeriques_mongodb_development" {
  dns_zone = var.dns_zone_incubateur
  name     = "mongodb.infrastructures-numeriques.dev"
  type     = "A"
  data     = "51.159.26.50"
  ttl      = 3600
}

module "infrastructures_numeriques" {
  source       = "./generic"
  common       = local.common
  project_name = "Infrastructures numeriques"
}
