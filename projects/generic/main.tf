resource "scaleway_domain_record" "record" {
  count    = var.with_record ? 1 : 0
  dns_zone = var.common.dns_zone_incubateur
  name     = local.project_subdomain
  type     = "CNAME"
  data     = var.common.production_cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "record_wildcard" {
  count    = var.with_wildcard_record ? 1 : 0
  dns_zone = var.common.dns_zone_incubateur
  name     = "*.${local.project_subdomain}"
  type     = "CNAME"
  data     = var.common.production_cluster_cname
  ttl      = 3600
}

module "grafana_production" {
  source = "../../modules/grafana_api_creation"
  grafana = {
    url     = var.common.grafana_production_url
    api_key = var.common.grafana_production_auth
  }
  project_name = local.project_name
}

module "grafana_development" {
  source = "../../modules/grafana_api_creation"
  grafana = {
    url     = var.common.grafana_development_url
    api_key = var.common.grafana_development_auth
  }
  project_name = local.project_name
}

module "project" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabterraformproject"
  version = "0.4.7"

  gitlab_project_location = var.common.projects_group_id
  project_name            = local.project_name
  project_slug            = local.project_slug

  dev_base-domain  = "${local.project_subdomain}.${var.common.development_base_domain}"
  prod_base-domain = "${local.project_subdomain}.${var.common.production_base_domain}"

  no_gitlab_tokens = true

  scaleway_organization_id                = var.common.scaleway_organization_id
  scaleway_cluster_development_cluster_id = var.common.scaleway_cluster_development_cluster_id
  scaleway_cluster_production_cluster_id  = var.common.scaleway_cluster_production_cluster_id

  grafana_development = {
    url            = var.common.grafana_development_url
    api_key        = module.grafana_development.api_key
    org_id         = module.grafana_development.org_id
    loki_url       = var.common.grafana_development_loki_url
    prometheus_url = var.common.grafana_development_prometheus_url
  }
  grafana_production = {
    url            = var.common.grafana_production_url
    api_key        = module.grafana_production.api_key
    org_id         = module.grafana_production.org_id
    loki_url       = var.common.grafana_production_loki_url
    prometheus_url = var.common.grafana_production_prometheus_url
  }
}

output "var_file" {
  value = module.project.var_file
}
