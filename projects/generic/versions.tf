terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
    }
    grafana = {
      source = "grafana/grafana"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}
