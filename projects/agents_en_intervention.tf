module "agents_en_intervention" {
  source               = "./generic"
  common               = local.common
  project_name         = "Agents En Intervention"
  project_slug         = "agents-intervention"
  with_record          = true
  with_wildcard_record = true
}
