resource "scaleway_domain_record" "dotations_locales" {
  dns_zone = var.dns_zone_incubateur
  name     = "dotations"
  type     = "CNAME"
  data     = "dotations-locales-app.osc-fr1.scalingo.io."
  ttl      = 3600
}

module "dotations_locales" {
  source               = "./generic"
  common               = local.common
  project_name         = "Dotations Locales"
  project_slug         = "dotationslocales"
  with_record          = false
  with_wildcard_record = false
}
