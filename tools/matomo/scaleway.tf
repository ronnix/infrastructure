resource "scaleway_object_bucket" "matomo_backups" {
  count = var.backup_bucket == null ? 0 : 1
  name  = var.backup_bucket
}
