variable "hostname" {
  type = string
}

variable "backup_bucket" {
  type    = string
  default = null
}

variable "scaleway_access_key" {
  type      = string
  sensitive = true
  default   = null
}

variable "scaleway_secret_key" {
  type      = string
  sensitive = true
  default   = null
}
