# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.15.1"
  constraints = "~> 3.15.1"
  hashes = [
    "h1:0Bis9e+rUXMy1hmO+sGGF15DNCvohoLvgP8/WQw9lVg=",
    "h1:uHVTET7NzTam/UfS9LvT21TNi2AxRf4WuHX4PBFeHOk=",
    "zh:1c3e89cf19118fc07d7b04257251fc9897e722c16e0a0df7b07fcd261f8c12e7",
    "zh:34d51d61089c9914bfd16d0a3944a44646c426ce6ef6d73276582ddf3bf5a3c5",
    "zh:379d7996519166f1d02c705e094e6d00a119ced61b4e9c21dd24b2c8fae95806",
    "zh:3f99fc5c897f9f7b232afb84bed06d8fd33f61b8b3b1e6dc4594fbaaf33014b9",
    "zh:50d2fcf01bfda0331843f2fe2889e913cdb87ba6878854f33db3c86de388967d",
    "zh:56b2ee60b833f32b01747eda335ce09363869af81f3d1f51f3c7aa2ee7de439e",
    "zh:6673490435366fe482b48de78605c38c648a27533c8fdc5cee24f7326b3c66fd",
    "zh:66c068fe2335dac8e9695049bdc3a950b324133afbad4ea8d5db0bbade089468",
    "zh:69a1266a1735bb0857307227fd42c1d5b5ccc2eb13595912d3b01b8c15ee3021",
    "zh:82de05003e83c23fe818210b17158ec50f1089befe84391f6113cbf05e4b15ca",
    "zh:9d74d8a3349cf4286746108b8a4516835d4a7c2ded2208f35b35582454885201",
    "zh:ac774f4b1fca6e849b88e6a4db87ec906f56b8f037f8e5f86a226bd2591da75f",
    "zh:d4ec3a46281401423ccdeaac83dd11a240834120e7446af4148050eba8758463",
    "zh:e2e17c338f9397aa69609b57b03caa44e2822d371d3dc9a021c441bce282bb89",
    "zh:e43d4c4e795a38cf6b8bf1f3b389706f4f66fd923f3657ff7a845bd8d9b9c8e1",
  ]
}

provider "registry.terraform.io/grafana/grafana" {
  version     = "1.28.2"
  constraints = "~> 1.28.2"
  hashes = [
    "h1:iWmL/sFw6oTJFLlhS6l0/CTj8MtmXu3TNYDuxpE0TiE=",
    "h1:t5/6GxropZKxNrzlLKJDRD8QSg38hYm2y0CD9NfuYhU=",
    "zh:037aa0ce2ac27f09a9727a6e74fad08a1592386ffb6099a775340aa8b6fe08bf",
    "zh:1554908741b0cd9513060441c15b1780e9533cd980743f19d857fd36c3909e64",
    "zh:24c427f9153d7d2105f9ccab588f00033af8c4bd71ce269fcbb968eeb3a664e1",
    "zh:2b418f3411345c04a9e1dcb68653ae6b82a4595bc0fdc3760ef8da65dea89443",
    "zh:3070a84eda2fb9f8a9b8047c47dcc82a93aff2e46031a4b3381f98bec80cdc8c",
    "zh:3d5ed4fe4597c3956611b283f0e3a827d2ee372f5f16feeb307fbea9145fc244",
    "zh:4605817458d001b9a4d5e3ee3afa773dbb0f02e8ba9377344daa561ec904dc6c",
    "zh:48c42667d0198dd6e3b4913d276f587c06ffcc75cbe7c2a8bbc379b9af8c1153",
    "zh:6b834b137debbfef6ffdbd2be5d20db5a89b1e79c0f83dac745523f320da0d1c",
    "zh:82bc5302f847ab3294708e55d80267a9ae044855c64475b3f6be1040a6340393",
    "zh:9bf3ecd23ca6d7239b64eafcbc7eec6ad7fb90b723b27d583bb371204d65b0dd",
    "zh:a7a901a195f04d6fc96129be1d75213aad340e1e0276add5ae786b940e95eaa3",
    "zh:aa05281fed6ebfd42d849abe9a0a038b22df2edb54b213d51209081d6a099863",
    "zh:e1df1caa32d10f0a16765ca09a358238deae442f117cd049bf88af96797f3641",
  ]
}

provider "registry.terraform.io/hashicorp/helm" {
  version     = "2.5.1"
  constraints = "~> 2.0, ~> 2.5.0"
  hashes = [
    "h1:NasRPC0qqlpGqcF3dsSoOFu7uc5hM+zJm+okd8FgrnQ=",
    "zh:140b9748f0ad193a20d69e59d672f3c4eda8a56cede56a92f931bd3af020e2e9",
    "zh:17ae319466ed6538ad49e011998bb86565fe0e97bc8b9ad7c8dda46a20f90669",
    "zh:3a8bd723c21ba70e19f0395ed7096fc8e08bfc23366f1c3f06a9107eb37c572c",
    "zh:3aae3b82adbe6dca52f1a1c8cf51575446e6b0f01f1b1f3b30de578c9af4a933",
    "zh:3f65221f40148df57d2888e4f31ef3bf430b8c5af41de0db39a2b964e1826d7c",
    "zh:650c74c4f46f5eb01df11d8392bdb7ebee3bba59ac0721000a6ad731ff0e61e2",
    "zh:930fb8ab4cd6634472dfd6aa3123f109ef5b32cbe6ef7b4695fae6751353e83f",
    "zh:ae57cd4b0be4b9ca252bc5d347bc925e35b0ed74d3dcdebf06c11362c1ac3436",
    "zh:d15b1732a8602b6726eac22628b2f72f72d98b75b9c6aabceec9fd696fda696a",
    "zh:d730ede1656bd193e2aea5302acec47c4905fe30b96f550196be4a0ed5f41936",
    "zh:f010d4f9d8cd15936be4df12bf256cb2175ca1dedb728bd3a866c03d2ee7591f",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.13.1"
  constraints = ">= 2.13.0, ~> 2.13, ~> 2.13.1"
  hashes = [
    "h1:cN3OwZvhtn/y3XfnGQ4hi+7oZp1gU2zVYhznRv2C7Qg=",
    "zh:061f6ecbbf9a3c6345b56c28ebc2966a05d8eb02f3ba56beedd66e4ea308e332",
    "zh:2119beeccb35bc5d1392b169f9fc748865261b45fb75fc8f57200e91658837c6",
    "zh:26c29083d0d84fbc2e356e3dd1db3e2dc4139e943acf7a318d3c98f954ac6bd6",
    "zh:2fb5823345ab05b3df74bb5c51c61072637d01b3cddffe3ad36a73b7d5b749e6",
    "zh:3475b4422fffaf58584c4d877f98bfeff075e4a746f13e985d2cb20adc873a6c",
    "zh:366b4bef49932d1d71b12849c1878c254a887962ff915f37982299c1185dd48a",
    "zh:589f9358e4a4bd74a83b97ccc64df455ddfa64c4c4e099aef30fa29080497a8a",
    "zh:7a0d75e0e4fee6cc5599ac9d5e91de563ce9ea7bd8137480c7abd09642a9e72c",
    "zh:a297a42aefe0650e3d9fbe55a3ee48b14bb8bb5edb7068c09512d72afc3d9ca5",
    "zh:b7f83a89b646542d02b733d464e45d6d0739a9dbb921305e7b8347e9fc98a149",
    "zh:d4c721174a598b66bd1b29c40fa7cffafe90bb58186cd7506d792a6b04161103",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version     = "3.3.2"
  constraints = "~> 3.3.0"
  hashes = [
    "h1:H5V+7iXol/EHB2+BUMzGlpIiCOdV74H8YjzCxnSAWcg=",
    "h1:YChjos7Hrvr2KgTc9GzQ+de/QE2VLAeRJgxFemnCltU=",
    "zh:038293aebfede983e45ee55c328e3fde82ae2e5719c9bd233c324cfacc437f9c",
    "zh:07eaeab03a723d83ac1cc218f3a59fceb7bbf301b38e89a26807d1c93c81cef8",
    "zh:427611a4ce9d856b1c73bea986d841a969e4c2799c8ac7c18798d0cc42b78d32",
    "zh:49718d2da653c06a70ba81fd055e2b99dfd52dcb86820a6aeea620df22cd3b30",
    "zh:5574828d90b19ab762604c6306337e6cd430e65868e13ef6ddb4e25ddb9ad4c0",
    "zh:7222e16f7833199dabf1bc5401c56d708ec052b2a5870988bc89ff85b68a5388",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:b1b2d7d934784d2aee98b0f8f07a8ccfc0410de63493ae2bf2222c165becf938",
    "zh:b8f85b6a20bd264fcd0814866f415f0a368d1123cd7879c8ebbf905d370babc8",
    "zh:c3813133acc02bbebddf046d9942e8ba5c35fc99191e3eb057957dafc2929912",
    "zh:e7a41dbc919d1de800689a81c240c27eec6b9395564630764ebb323ea82ac8a9",
    "zh:ee6d23208449a8eaa6c4f203e33f5176fa795b4b9ecf32903dffe6e2574732c2",
  ]
}

provider "registry.terraform.io/hashicorp/time" {
  version = "0.9.1"
  hashes = [
    "h1:NUv/YtEytDQncBQ2mTxnUZEy/rmDlPYmE9h2iokR0vk=",
    "h1:VxyoYYOCaJGDmLz4TruZQTSfQhvwEcMxvcKclWdnpbs=",
    "zh:00a1476ecf18c735cc08e27bfa835c33f8ac8fa6fa746b01cd3bcbad8ca84f7f",
    "zh:3007f8fc4a4f8614c43e8ef1d4b0c773a5de1dcac50e701d8abc9fdc8fcb6bf5",
    "zh:5f79d0730fdec8cb148b277de3f00485eff3e9cf1ff47fb715b1c969e5bbd9d4",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:8c8094689a2bed4bb597d24a418bbbf846e15507f08be447d0a5acea67c2265a",
    "zh:a6d9206e95d5681229429b406bc7a9ba4b2d9b67470bda7df88fa161508ace57",
    "zh:aa299ec058f23ebe68976c7581017de50da6204883950de228ed9246f309e7f1",
    "zh:b129f00f45fba1991db0aa954a6ba48d90f64a738629119bfb8e9a844b66e80b",
    "zh:ef6cecf5f50cda971c1b215847938ced4cb4a30a18095509c068643b14030b00",
    "zh:f1f46a4f6c65886d2dd27b66d92632232adc64f92145bf8403fe64d5ffa5caea",
    "zh:f79d6155cda7d559c60d74883a24879a01c4d5f6fd7e8d1e3250f3cd215fb904",
    "zh:fd59fa73074805c3575f08cd627eef7acda14ab6dac2c135a66e7a38d262201c",
  ]
}

provider "registry.terraform.io/hashicorp/tls" {
  version = "4.0.4"
  hashes = [
    "h1:GZcFizg5ZT2VrpwvxGBHQ/hO9r6g0vYdQqx3bFD3anY=",
    "h1:pe9vq86dZZKCm+8k1RhzARwENslF3SXb9ErHbQfgjXU=",
    "zh:23671ed83e1fcf79745534841e10291bbf34046b27d6e68a5d0aab77206f4a55",
    "zh:45292421211ffd9e8e3eb3655677700e3c5047f71d8f7650d2ce30242335f848",
    "zh:59fedb519f4433c0fdb1d58b27c210b27415fddd0cd73c5312530b4309c088be",
    "zh:5a8eec2409a9ff7cd0758a9d818c74bcba92a240e6c5e54b99df68fff312bbd5",
    "zh:5e6a4b39f3171f53292ab88058a59e64825f2b842760a4869e64dc1dc093d1fe",
    "zh:810547d0bf9311d21c81cc306126d3547e7bd3f194fc295836acf164b9f8424e",
    "zh:824a5f3617624243bed0259d7dd37d76017097dc3193dac669be342b90b2ab48",
    "zh:9361ccc7048be5dcbc2fafe2d8216939765b3160bd52734f7a9fd917a39ecbd8",
    "zh:aa02ea625aaf672e649296bce7580f62d724268189fe9ad7c1b36bb0fa12fa60",
    "zh:c71b4cd40d6ec7815dfeefd57d88bc592c0c42f5e5858dcc88245d371b4b8b1e",
    "zh:dabcd52f36b43d250a3d71ad7abfa07b5622c69068d989e60b79b2bb4f220316",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/olivr/gpg" {
  version = "0.2.1"
  hashes = [
    "h1:+IxSHXkyhHgszKVym4qfp9rF8U0a1a2But15WdA1Xlg=",
    "h1:1aDT6MVlRnC8qF68rHe1NhCdzz/+tWfix7Aftl3s/qw=",
    "zh:014bb7c1140dd2bb96b13a729d450e0aaa743b7f233884ab471a1e029853e66b",
    "zh:262fafa02f79402373b34c7a3bcb1b7b611a6e1fb6713b38480359d8582670e8",
    "zh:5202f998398f8b0d0a9cce7c1dd891ecce493b7aeb23d970ba5319cfbb024086",
    "zh:63609d1e453442453834bf7196e79d29d9340c76f172aec21c321aa13880011a",
    "zh:7fda5a60fe84c4e592087befdbba0b28f0b7f2218d81474720a79bfc587745e8",
    "zh:7fe14800b15f3a79efe9222047c34d0347c0a904356205c9a38abb5a668bc1cc",
    "zh:85f80b1a074b1e616c15c78d6d97c802ee0b5bd2bd878febc8f8430467de9819",
    "zh:a38ba53bd08827ef648f082efb216f65174ab2b18b5d6e14daf9b15ff8a9dfe8",
    "zh:a7c5c35cac6466d60feabb5e2e27a8d582f104152b7e5b87adb0071108b413a6",
    "zh:b6983ea1502698a74215fe2a52b3b81b48dc539daf5d9abe681ef9bcd9b67453",
    "zh:da2a64d029a496d20cfd23bc63a6943e8b80ceead96f5967a2e040eb5e1aeac4",
    "zh:e12b1fb462ab0df7cea32da5f3263d7c10305419dc86118e41687bb863737324",
    "zh:e15335ccd1589cccfa539f0b17b7736b57175ce0ac2e0aad87800b492ee066bf",
    "zh:ebc1e1032e6c996cb3295977196ee87f8babbf38c427bcbd2d8eca41d65f9ffd",
  ]
}

provider "registry.terraform.io/scaleway/scaleway" {
  version     = "2.7.1"
  constraints = "~> 2.7.1"
  hashes = [
    "h1:DpBpdi+JyQuxPorljzwkWn4XTnAkFvdRHxgJ4Bsn54A=",
    "h1:m8s3LpHzJbpPCHwLIlXekJ1IJXcLXoodmSI2gRFy6bM=",
    "zh:1d57366080ea6aa2ad9c0f7760edce8a7f5c5186f5e7d499eda650dc9702f750",
    "zh:304bfe31dea64c9be8568af350d0339e79acb4953739d57b0a67161d07452539",
    "zh:312c1113336e95bb4f265a39e54572ba38b1ba035f49776c4334229907bec3c0",
    "zh:45024e1db74616d3903423057e81cf483e580c7959b8b1bf0b69df7d07e2abae",
    "zh:4f83929d732ced45796342452091b1daf16011fed5e27373848fa0de552cff57",
    "zh:730b9aee4aa3ffb4c16e012e7128f634575153074e7f94b8d95ec47295fab8a9",
    "zh:7e6c2c9c877709e4d9f19fea5418e6231fbb59adc058bad5751b9e7a93901669",
    "zh:89a222fc21b96333dff5e662bffdacc73a69ca40e1098a2ea7a5b469db3ef339",
    "zh:9a524b112de8bb91b7074f3249c1ece5ce2ea3be5406b61d57b5c9c34ada9381",
    "zh:aac9dc8c1e2c8aa8f45bad456333cc8310309637cf25f3f897bde9517e99c67e",
    "zh:b85ad3591d69cc81d876bd992b5a8cb407f6cb624f01629d27cede3856b3c53a",
    "zh:e29871c2f7e9054b0bf5ee1561668c2071977c263c8141a57ae9e818596c1aa7",
    "zh:ee2c9ad5a2f63fe604662c246a80b11122a6a9f12354b05a2d615c38bed5686e",
    "zh:f0da1f79be0cd6965f6710cf71d29dad67f2360aaf8824538512dc327a6bb03c",
  ]
}
