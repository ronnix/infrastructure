resource "scaleway_k8s_cluster" "production" {
  name        = "Incubateur-production"
  description = "Managed by terraform"
  version     = "1.24.3"
  cni         = "calico"
  tags        = ["production", "terraform"]
  project_id  = var.scaleway_project_id
  lifecycle {
    prevent_destroy = true
  }
}

resource "scaleway_k8s_pool" "default" {
  cluster_id          = scaleway_k8s_cluster.production.id
  name                = "default"
  node_type           = "GP1-S"
  size                = 2
  autohealing         = true
  wait_for_pool_ready = true
  autoscaling         = true
  min_size            = 3
  max_size            = 10
  lifecycle {
    prevent_destroy = true
  }
}
