terraform {
  required_providers {
    helm = {
      source = "hashicorp/helm"
    }
    scaleway = {
      source = "scaleway/scaleway"
    }
  }
}
