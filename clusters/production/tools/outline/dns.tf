resource "scaleway_domain_record" "outline" {
  provider = scaleway.default-project
  dns_zone = var.dns_zone_incubateur
  name     = "outline"
  type     = "CNAME"
  data     = var.cluster_cname
  ttl      = 3600
}

locals {
  hostname = "${scaleway_domain_record.outline.name}.${scaleway_domain_record.outline.dns_zone}"
}
