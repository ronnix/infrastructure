resource "scaleway_domain_record" "chat" {
  dns_zone = var.dns_zone_incubateur
  name     = "chat"
  type     = "CNAME"
  data     = var.cluster_cname
  ttl      = 3600
}

locals {
  hostname = "${scaleway_domain_record.chat.name}.${scaleway_domain_record.chat.dns_zone}"
}
