locals {
  project_name      = "DecidimInclusionNumerique"
  project_slug      = "decidim-inclusion-numerique"
  backup_offset_min = 20
}

module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "1.0.1"
  max_cpu_requests  = "9"
  max_memory_limits = "20Gi"
  namespace         = local.project_slug
  project_slug      = local.project_slug
  project_name      = local.project_name

  default_container_cpu_requests  = "200m"
  default_container_memory_limits = "128Mi"
}

resource "random_password" "decidim_here_api_key" {
  length = 43
}
resource "random_password" "decidim_secret_key" {
  length  = 128
  special = false
}

module "postgresql" {
  source                                = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version                               = "0.0.22"
  namespace                             = module.namespace.namespace
  chart_name                            = "postgresql"
  pg_replicas                           = 3
  pg_volume_size                        = "5Gi"
  pg_backups_volume_enabled             = true
  pg_backups_volume_size                = "20Gi"
  pg_backups_volume_full_schedule       = "${local.backup_offset_min} 2 * * 0"
  pg_backups_volume_incr_schedule       = "${local.backup_offset_min} 2 * * 1-6"
  pg_backups_volume_full_retention      = 4
  pg_backups_volume_full_retention_type = "count"
  pg_backups_s3_enabled                 = true
  pg_backups_s3_bucket                  = resource.scaleway_object_bucket.decidim_backups.name
  pg_backups_s3_region                  = resource.scaleway_object_bucket.decidim_backups.region
  pg_backups_s3_endpoint                = "s3.${resource.scaleway_object_bucket.decidim_backups.region}.scw.cloud"
  pg_backups_s3_access_key              = var.scaleway_access_key
  pg_backups_s3_secret_key              = var.scaleway_secret_key
  pg_backups_s3_full_schedule           = "${local.backup_offset_min} 3 * * 0"
  pg_backups_s3_incr_schedule           = "${local.backup_offset_min} 3 * * 1-6"
}

module "decidim" {
  source           = "gitlab.com/vigigloo/tools-k8s/decidim"
  version          = "1.0.0"
  namespace        = module.namespace.namespace
  chart_name       = "decidim"
  chart_version    = "1.0.0"
  image_repository = "registry.gitlab.com/incubateur-territoires/incubateur/decidim-ditp"
  image_tag        = "0.1.1"
  values = [
    file("${path.module}/decidim.yaml"),
    <<-EOT
    extraEnv:
      GEOCODER_LOOKUP_API_KEY: ${random_password.decidim_here_api_key.result}
    EOT
  ]
  ingress_host = var.decidim_host

  decidim_secret_key_base = random_password.decidim_secret_key.result

  decidim_postgresql_database = module.postgresql.dbname
  decidim_postgresql_host     = module.postgresql.host
  decidim_postgresql_username = module.postgresql.user
  decidim_postgresql_password = replace(module.postgresql.password, ",", "\\,")
  decidim_postgresql_port     = module.postgresql.port

  decidim_s3_bucket            = scaleway_object_bucket.decidim.name
  decidim_s3_host              = "${scaleway_object_bucket.decidim.name}.s3.${scaleway_object_bucket.decidim.region}.scw.cloud"
  decidim_s3_region            = scaleway_object_bucket.decidim.region
  decidim_s3_access_key_id     = var.scaleway_access_key
  decidim_s3_secret_access_key = var.scaleway_secret_key

  decidim_redis_enabled = true
  decidim_redis_url     = "redis://${module.redis.hostname}"

  decidim_email_smtp_host     = "smtp-relay.sendinblue.com"
  decidim_email_smtp_password = var.decidim_smtp_password
  decidim_email_smtp_user     = var.decidim_smtp_user
  decidim_email_smtp_secure   = false
  decidim_email_smtp_port     = 587

  requests_cpu    = "10m"
  requests_memory = "1200Mi"
  limits_memory   = "2Gi"
}

module "redis" {
  source         = "gitlab.com/vigigloo/tools-k8s/redis"
  version        = "0.1.1"
  namespace      = module.namespace.namespace
  chart_name     = "redis"
  redis_password = "redis"
  redis_replicas = 0
  values         = [file("${path.module}/redis.yaml")]

  requests_cpu    = "20m"
  requests_memory = "40Mi"
}
