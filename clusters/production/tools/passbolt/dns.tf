resource "scaleway_domain_record" "domain_record" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "passbolt"
  type     = "CNAME"
  data     = var.cluster_cname
  ttl      = 3600
}

locals {
  hostname = "${scaleway_domain_record.domain_record.name}.${scaleway_domain_record.domain_record.dns_zone}"
}
