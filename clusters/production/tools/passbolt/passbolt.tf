locals {
  project_name = "Passbolt"
  project_slug = "passbolt"
}

module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "1.0.1"
  namespace         = local.project_slug
  max_memory_limits = "14Gi"
  max_cpu_requests  = "9"
  project_name      = local.project_name
  project_slug      = local.project_slug

  default_container_cpu_requests  = "20m"
  default_container_memory_limits = "16Mi"
}

resource "gpg_private_key" "passbolt_gpg" {
  name     = "Passbolt Incubateur des Territoires"
  email    = var.gpg_key_email
  rsa_bits = 4096
}

resource "tls_private_key" "passbolt_jwt" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

module "passbolt" {
  source     = "gitlab.com/vigigloo/tools-k8s/passbolt"
  version    = "0.1.2"
  namespace  = module.namespace.namespace
  chart_name = "passbolt"
  values = [
    file("${path.module}/passbolt.yaml"),
  ]
  requests_cpu  = "50m"
  limits_memory = "1Gi"

  passbolt_host = local.hostname

  passbolt_database_host     = module.postgresql.host
  passbolt_database_port     = module.postgresql.port
  passbolt_database_username = module.postgresql.user
  passbolt_database_password = replace(module.postgresql.password, ",", "\\,")
  passbolt_database_name     = module.postgresql.dbname

  passbolt_gpg_private_key = gpg_private_key.passbolt_gpg.private_key
  passbolt_gpg_public_key  = gpg_private_key.passbolt_gpg.public_key
  passbolt_gpg_fingerprint = gpg_private_key.passbolt_gpg.fingerprint

  passbolt_jwt_private_key = tls_private_key.passbolt_jwt.private_key_pem
  passbolt_jwt_public_key  = tls_private_key.passbolt_jwt.public_key_pem

  passbolt_smtp_host       = var.smtp_host
  passbolt_smtp_port       = 587
  passbolt_smtp_username   = var.smtp_username
  passbolt_smtp_password   = var.smtp_password
  passbolt_smtp_tls        = true
  passbolt_smtp_from_email = var.smtp_from_email
  passbolt_smtp_from_name  = "Passbolt Incubateur des Territoires"
}
