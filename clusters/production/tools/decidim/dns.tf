resource "scaleway_domain_record" "decidim" {
  provider = scaleway.default-project
  dns_zone = var.dns_zone_incubateur
  name     = "mon"
  type     = "CNAME"
  data     = var.cluster_cname
  ttl      = 3600
}

locals {
  hostname = "${scaleway_domain_record.decidim.name}.${scaleway_domain_record.decidim.dns_zone}"
}
