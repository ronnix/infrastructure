module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "1.0.1"
  max_cpu_requests  = "9"
  max_memory_limits = "20Gi"
  namespace         = "decidim"
  project_name      = "Decidim"
  project_slug      = "decidim"

  default_container_cpu_requests  = "200m"
  default_container_memory_limits = "128Mi"
}

resource "scaleway_object_bucket" "decidim" {
  name   = "anct-decidim"
  region = "fr-par"
}
resource "scaleway_object_bucket_acl" "decidim" {
  bucket = scaleway_object_bucket.decidim.name
  acl    = "public-read"
}

module "postgresql" {
  source     = "gitlab.com/vigigloo/tools-k8s/postgresql"
  version    = "0.1.0"
  chart_name = "database"
  namespace  = module.namespace.namespace
}

resource "random_string" "decidim_secret" {
  length  = 128
  special = false
  upper   = false
  lifecycle {
    ignore_changes = all
  }
}

module "decidim" {
  source                  = "gitlab.com/vigigloo/tools-k8s/decidim"
  version                 = "0.0.5"
  chart_name              = "decidim"
  chart_version           = "0.1.2"
  namespace               = module.namespace.namespace
  image_repository        = "registry.gitlab.com/incubateur-territoires/incubateur/decidim"
  image_tag               = "v0.0.2"
  decidim_secret_key_base = random_string.decidim_secret.result
  ingress_host            = local.hostname
  values = [
    file("${path.module}/decidim.yaml")
  ]
  decidim_postgresql_host     = module.postgresql.domain
  decidim_postgresql_database = module.postgresql.database
  decidim_postgresql_username = module.postgresql.superuser-username
  decidim_postgresql_password = module.postgresql.superuser-password

  decidim_s3_bucket            = scaleway_object_bucket.decidim.name
  decidim_s3_host              = "${scaleway_object_bucket.decidim.name}.s3.${scaleway_object_bucket.decidim.region}.scw.cloud"
  decidim_s3_region            = scaleway_object_bucket.decidim.region
  decidim_s3_access_key_id     = var.scaleway_access_key
  decidim_s3_secret_access_key = var.scaleway_secret_key

  decidim_redis_url = "redis://${helm_release.redis.metadata[0].name}-master.${helm_release.redis.metadata[0].namespace}:6379"

  requests_cpu = "400m"
}

moved {
  from = random_string.decidim-secret
  to   = random_string.decidim_secret
}
