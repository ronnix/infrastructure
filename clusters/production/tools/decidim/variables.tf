variable "dns_zone_incubateur" {
  type = string
}
variable "cluster_cname" {
  type = string
}

variable "scaleway_access_key" {
  type = string
}

variable "scaleway_secret_key" {
  type      = string
  sensitive = true
}

variable "scaleway_default_access_key" {
  type = string
}

variable "scaleway_default_secret_key" {
  type      = string
  sensitive = true
}

variable "scaleway_default_project_id" {
  type = string
}
