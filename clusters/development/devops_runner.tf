resource "scaleway_object_bucket" "gitlab_cache" {
  name   = "devops-gitlab-runner-cache"
  region = "fr-par"
}

module "devops_gitlab_runner" {
  source  = "gitlab.com/vigigloo/tf-modules/k8sgitlabrunner"
  version = "0.2.6"

  kubeconfig   = scaleway_k8s_cluster.development.kubeconfig[0]
  project_slug = "devops-infra"
  project_name = "devOpsInfra"

  chart_version = "0.47.1"

  gitlab_groups = [
    13501540, // https://gitlab.com/incubateur-territoires/devops
  ]

  cache_provider         = "s3"
  cache_s3_host          = "s3.fr-par.scw.cloud"
  cache_s3_bucket-name   = scaleway_object_bucket.gitlab_cache.name
  cache_s3_bucket-region = scaleway_object_bucket.gitlab_cache.region
  cache_s3_access-key    = var.scaleway_access_key
  cache_s3_secret-key    = var.scaleway_secret_key
}

module "projects_gitlab_runner" {
  source  = "gitlab.com/vigigloo/tf-modules/k8sgitlabrunner"
  version = "0.2.3"

  kubeconfig   = scaleway_k8s_cluster.development.kubeconfig[0]
  project_slug = "devops-projects"
  project_name = "devOpsProjects"

  chart_version = "0.47.1"

  gitlab_groups = [
    52940215, // https://gitlab.com/incubateur-territoires/ateliers
    15978123, // https://gitlab.com/incubateur-territoires/france-relance
    13506047, // https://gitlab.com/incubateur-territoires/incubateur
  ]
}

module "global_docker_runner" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabinstancerunner"
  version = "0.0.8"

  privileged            = true
  enable_ipv4           = true
  enable_ipv6           = true
  gitlab_runner-version = "15.6.1"
  gitlab_tags           = ["dind", "scaleway", "alpine"]

  gitlab_groups = [
    13501534, //https://gitlab.com/incubateur-territoires
  ]
}

moved {
  from = module.devops-gitlab-runner
  to   = module.devops_gitlab_runner
}
moved {
  from = module.projects-gitlab-runner
  to   = module.projects_gitlab_runner
}
moved {
  from = module.global-docker-runner
  to   = module.global_docker_runner
}
