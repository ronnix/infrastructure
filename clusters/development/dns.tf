locals {
  cluster_cname = "${scaleway_domain_record.cluster_cname.name}.${scaleway_domain_record.cluster_cname.dns_zone}."
}
resource "scaleway_domain_record" "cluster_cname" {
  provider = scaleway.default-project
  dns_zone = var.dns_zone_incubateur
  name     = "lb-kubernetes-dev"
  type     = "A"
  data     = scaleway_lb_ip.haproxy_ip.ip_address
  ttl      = 3600
}

resource "scaleway_domain_record" "dev" {
  provider = scaleway.default-project
  dns_zone = var.dns_zone_incubateur
  name     = "dev"
  type     = "CNAME"
  data     = local.cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "dev_wildcard" {
  provider = scaleway.default-project
  dns_zone = var.dns_zone_incubateur
  name     = "*.dev"
  type     = "CNAME"
  data     = local.cluster_cname
  ttl      = 3600
}
